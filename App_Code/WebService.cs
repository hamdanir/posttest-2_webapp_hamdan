﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{

    public WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    // Company//
    [WebMethod]
    public void Company()
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                var company = db.TB_Companies.Select(x => new
                { 
                     x.ID, 
                     x.UID,
                    x.Name,
                     x.Address,
                     x.Telephone,
                     x.FLag,
                     x.CreatedBy
                
                }).ToArray();

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = company,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("Error") ? ex.Message : "Error Company"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void CompanybyUID(string UID)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                var company = db.TB_Companies.Where(x => x.UID.ToString() == UID).Select(x => new
                    
                {
                     x.ID,
                     x.UID,
                     x.Name,
                     x.Address,
                     x.Telephone,
                    x.FLag,
                    x.CreatedBy

                }).ToArray();

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = company,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("Error") ? ex.Message : "Error Company"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void CreateCompany(string name, string address, string telephone, int flag, int createdby)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                    ControllerCompany controllerCompany = new ControllerCompany(db);
                    var company = controllerCompany.Create(name, address, telephone, flag, createdby);
                
                    db.SubmitChanges();
                

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = company,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("Error") ? ex.Message : "Error Company"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void UpdateCompany(string uid,string name, string address, string telephone, int flag, int createdby)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);

                var company = controllerCompany.Update(uid, name, address, telephone, flag, createdby);

                db.SubmitChanges();


                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = company,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("Error") ? ex.Message : "Error Company"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void DeleteCompany(string UID)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";
        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {

                ControllerCompany controllerCompany = new ControllerCompany(db);
                var company = controllerCompany.Delete(UID);
                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = company,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));


            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("error") ? ex.Message : "error company"
                }

            }, Formatting.Indented));
        }
    }

    //Company//

    //Position
    [WebMethod]
    public void Position()
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                var position = db.TB_Positions.Select(x => new
                {
                    x.ID,
                    x.UID,
                    x.Name,
                    x.CreatedBy

                }).ToArray();

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = position,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("Error") ? ex.Message : "Error Position"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void PositionbyUID(string UID)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                var position = db.TB_Positions.Where(x => x.UID.ToString() == UID).Select(x => new

                {
                    x.ID,
                    x.UID,
                    x.Name,
                    x.CreatedBy

                }).ToArray();

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = position,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("Error") ? ex.Message : "Error Position"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void CreatePosition(string name, int createdby)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerPosition controllerPosition = new ControllerPosition(db);
                var position = controllerPosition.Create(name);

                db.SubmitChanges();


                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = position,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("Error") ? ex.Message : "Error Position"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void UpdatePosition(string uid, string name)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerPosition controllerPosition = new ControllerPosition(db);

                var position = controllerPosition.Update(uid, name);

                db.SubmitChanges();


                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = position,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("Error") ? ex.Message : "Error Position"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void DeletePosition(string UID)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";
        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {

                ControllerPosition controllerPosition = new ControllerPosition(db);
                var position = controllerPosition.Delete(UID);
                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = position,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));


            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("error") ? ex.Message : "error Position"
                }

            }, Formatting.Indented));
        }
    }
    //Positiom





    //User//
    [WebMethod]
    public void User()
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                var user = db.TBUsers.Select(x => new
                {
                    x.ID,
                    x.UID,
                    x.IDCompany,
                    x.IDPosition,
                    NameCompany = x.TB_Company.Name,
                    NamePosition = x.TB_Position.Name,
                    x.Name,
                    x.Address,
                    x.Email,
                    x.Telephone,
                    x.Username,
                    x.Password,
                    x.Role,
                    x.Flag,
                    x.CreatedBy

                }).ToArray();

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = user,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("Error") ? ex.Message : "Error User"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void GetUserByUID(string UID)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";
        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                var user = db.TBUsers.Where(x => x.UID.ToString() == UID).Select(x => new
                {
                    x.ID,
                    x.UID,
                    x.IDCompany,
                    x.IDPosition,
                    x.Name,
                    x.Address,
                    x.Telephone,
                    x.Email,
                    x.Username,
                    x.Password,
                    x.Role,
                    x.Flag,
                    x.CreatedBy
                }).ToArray();

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = user,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = "Success get"
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("error") ? ex.Message : "error company"
                }

            }, Formatting.Indented));
        }
    }


    [WebMethod]
    public void CreateUser(int IDCompany, int IDPosition, string name, string address, string email, string telephone,string username, string password, string role )
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerUser controllerUser = new ControllerUser(db);
                var user = controllerUser.Create(IDCompany, IDPosition, name, address, email, telephone, username, password, role);

                db.SubmitChanges();


                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = user,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = ""
                    }
                }, Formatting.Indented));
            }
        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("Error") ? ex.Message : "Error Company"
                }
            }, Formatting.Indented));
        }
    }

    [WebMethod]
    public void UpdateUser(int ID, int IDCompany, int IDPosition, string name, string address, string email, string telephone, string username, string password, string role)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerUser controllerUser = new ControllerUser(db);
                var user = controllerUser.Update(ID, IDCompany, IDPosition, name, address, email, telephone, username, password, role);
                db.SubmitChanges();
                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = user,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = "Success Update User"
                    }
                }, Formatting.Indented));
            }

        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("error") ? ex.Message : "error update user"
                }

            }, Formatting.Indented));
        }
    }


    [WebMethod]
    public void DeleteUser(int ID)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";

        try
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerUser controllerUser = new ControllerUser(db);
                var user = controllerUser.Delete(ID);

                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    Data = user,
                    Result = new WebServiceResult
                    {
                        EnumWebService = (int)EnumWebService.Success,
                        Pesan = "Success Delete User"
                    }
                }, Formatting.Indented));
            }

        }
        catch (Exception ex)
        {
            Context.Response.Write(JsonConvert.SerializeObject(new
            {
                Data = (string)null,
                Result = new WebServiceResult
                {
                    EnumWebService = (int)EnumWebService.Exception,
                    Pesan = ex.Message.StartsWith("error") ? ex.Message : "error delete user"
                }

            }, Formatting.Indented));
        }


    }


    //User//
}
