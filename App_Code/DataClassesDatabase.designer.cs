﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;



[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="PreTestHamdan")]
public partial class DataClassesDatabaseDataContext : System.Data.Linq.DataContext
{
	
	private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
	
  #region Extensibility Method Definitions
  partial void OnCreated();
  partial void InsertTB_Company(TB_Company instance);
  partial void UpdateTB_Company(TB_Company instance);
  partial void DeleteTB_Company(TB_Company instance);
  partial void InsertTB_Document(TB_Document instance);
  partial void UpdateTB_Document(TB_Document instance);
  partial void DeleteTB_Document(TB_Document instance);
  partial void InsertTB_DocumentCatagory(TB_DocumentCatagory instance);
  partial void UpdateTB_DocumentCatagory(TB_DocumentCatagory instance);
  partial void DeleteTB_DocumentCatagory(TB_DocumentCatagory instance);
  partial void InsertTB_Position(TB_Position instance);
  partial void UpdateTB_Position(TB_Position instance);
  partial void DeleteTB_Position(TB_Position instance);
  partial void InsertTBUser(TBUser instance);
  partial void UpdateTBUser(TBUser instance);
  partial void DeleteTBUser(TBUser instance);
  #endregion
	
	public DataClassesDatabaseDataContext() :
            base(global::System.Configuration.ConfigurationManager.ConnectionStrings["PreTestHamdanConnectionString"].ConnectionString, mappingSource)

        
	{
		OnCreated();
	}
	
	public DataClassesDatabaseDataContext(System.Data.IDbConnection connection) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public DataClassesDatabaseDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public DataClassesDatabaseDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public System.Data.Linq.Table<TB_Company> TB_Companies
	{
		get
		{
			return this.GetTable<TB_Company>();
		}
	}
	
	public System.Data.Linq.Table<TB_Document> TB_Documents
	{
		get
		{
			return this.GetTable<TB_Document>();
		}
	}
	
	public System.Data.Linq.Table<TB_DocumentCatagory> TB_DocumentCatagories
	{
		get
		{
			return this.GetTable<TB_DocumentCatagory>();
		}
	}
	
	public System.Data.Linq.Table<TB_Position> TB_Positions
	{
		get
		{
			return this.GetTable<TB_Position>();
		}
	}
	
	public System.Data.Linq.Table<TBUser> TBUsers
	{
		get
		{
			return this.GetTable<TBUser>();
		}
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_Company")]
public partial class TB_Company : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _ID;
	
	private System.Nullable<System.Guid> _UID;
	
	private string _Name;
	
	private string _Address;
	
	private string _Email;
	
	private string _Telephone;
	
	private System.Nullable<int> _FLag;
	
	private System.Nullable<int> _CreatedBy;
	
	private System.Nullable<System.DateTime> _CreatedAt;
	
	private EntitySet<TB_Document> _TB_Documents;
	
	private EntitySet<TBUser> _TBUsers;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnUIDChanging(System.Nullable<System.Guid> value);
    partial void OnUIDChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnAddressChanging(string value);
    partial void OnAddressChanged();
    partial void OnEmailChanging(string value);
    partial void OnEmailChanged();
    partial void OnTelephoneChanging(string value);
    partial void OnTelephoneChanged();
    partial void OnFLagChanging(System.Nullable<int> value);
    partial void OnFLagChanged();
    partial void OnCreatedByChanging(System.Nullable<int> value);
    partial void OnCreatedByChanged();
    partial void OnCreatedAtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreatedAtChanged();
    #endregion
	
	public TB_Company()
	{
		this._TB_Documents = new EntitySet<TB_Document>(new Action<TB_Document>(this.attach_TB_Documents), new Action<TB_Document>(this.detach_TB_Documents));
		this._TBUsers = new EntitySet<TBUser>(new Action<TBUser>(this.attach_TBUsers), new Action<TBUser>(this.detach_TBUsers));
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int ID
	{
		get
		{
			return this._ID;
		}
		set
		{
			if ((this._ID != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._ID = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UID", DbType="UniqueIdentifier")]
	public System.Nullable<System.Guid> UID
	{
		get
		{
			return this._UID;
		}
		set
		{
			if ((this._UID != value))
			{
				this.OnUIDChanging(value);
				this.SendPropertyChanging();
				this._UID = value;
				this.SendPropertyChanged("UID");
				this.OnUIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="VarChar(255) NOT NULL", CanBeNull=false)]
	public string Name
	{
		get
		{
			return this._Name;
		}
		set
		{
			if ((this._Name != value))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._Name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Address", DbType="Text", UpdateCheck=UpdateCheck.Never)]
	public string Address
	{
		get
		{
			return this._Address;
		}
		set
		{
			if ((this._Address != value))
			{
				this.OnAddressChanging(value);
				this.SendPropertyChanging();
				this._Address = value;
				this.SendPropertyChanged("Address");
				this.OnAddressChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Email", DbType="VarChar(50)")]
	public string Email
	{
		get
		{
			return this._Email;
		}
		set
		{
			if ((this._Email != value))
			{
				this.OnEmailChanging(value);
				this.SendPropertyChanging();
				this._Email = value;
				this.SendPropertyChanged("Email");
				this.OnEmailChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Telephone", DbType="VarChar(14)")]
	public string Telephone
	{
		get
		{
			return this._Telephone;
		}
		set
		{
			if ((this._Telephone != value))
			{
				this.OnTelephoneChanging(value);
				this.SendPropertyChanging();
				this._Telephone = value;
				this.SendPropertyChanged("Telephone");
				this.OnTelephoneChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_FLag", DbType="Int")]
	public System.Nullable<int> FLag
	{
		get
		{
			return this._FLag;
		}
		set
		{
			if ((this._FLag != value))
			{
				this.OnFLagChanging(value);
				this.SendPropertyChanging();
				this._FLag = value;
				this.SendPropertyChanged("FLag");
				this.OnFLagChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="Int")]
	public System.Nullable<int> CreatedBy
	{
		get
		{
			return this._CreatedBy;
		}
		set
		{
			if ((this._CreatedBy != value))
			{
				this.OnCreatedByChanging(value);
				this.SendPropertyChanging();
				this._CreatedBy = value;
				this.SendPropertyChanged("CreatedBy");
				this.OnCreatedByChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedAt", DbType="DateTime")]
	public System.Nullable<System.DateTime> CreatedAt
	{
		get
		{
			return this._CreatedAt;
		}
		set
		{
			if ((this._CreatedAt != value))
			{
				this.OnCreatedAtChanging(value);
				this.SendPropertyChanging();
				this._CreatedAt = value;
				this.SendPropertyChanged("CreatedAt");
				this.OnCreatedAtChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TB_Company_TB_Document", Storage="_TB_Documents", ThisKey="ID", OtherKey="IDCompany")]
	public EntitySet<TB_Document> TB_Documents
	{
		get
		{
			return this._TB_Documents;
		}
		set
		{
			this._TB_Documents.Assign(value);
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TB_Company_TBUser", Storage="_TBUsers", ThisKey="ID", OtherKey="IDCompany")]
	public EntitySet<TBUser> TBUsers
	{
		get
		{
			return this._TBUsers;
		}
		set
		{
			this._TBUsers.Assign(value);
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	
	private void attach_TB_Documents(TB_Document entity)
	{
		this.SendPropertyChanging();
		entity.TB_Company = this;
	}
	
	private void detach_TB_Documents(TB_Document entity)
	{
		this.SendPropertyChanging();
		entity.TB_Company = null;
	}
	
	private void attach_TBUsers(TBUser entity)
	{
		this.SendPropertyChanging();
		entity.TB_Company = this;
	}
	
	private void detach_TBUsers(TBUser entity)
	{
		this.SendPropertyChanging();
		entity.TB_Company = null;
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_Document")]
public partial class TB_Document : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _ID;
	
	private System.Nullable<System.Guid> _UID;
	
	private int _IDCompany;
	
	private int _IDCategory;
	
	private string _Name;
	
	private string _Description;
	
	private System.Nullable<int> _Flag;
	
	private System.Nullable<int> _CreatedBy;
	
	private System.Nullable<System.DateTime> _CreatedAt;
	
	private EntityRef<TB_Company> _TB_Company;
	
	private EntityRef<TB_DocumentCatagory> _TB_DocumentCatagory;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnUIDChanging(System.Nullable<System.Guid> value);
    partial void OnUIDChanged();
    partial void OnIDCompanyChanging(int value);
    partial void OnIDCompanyChanged();
    partial void OnIDCategoryChanging(int value);
    partial void OnIDCategoryChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnDescriptionChanging(string value);
    partial void OnDescriptionChanged();
    partial void OnFlagChanging(System.Nullable<int> value);
    partial void OnFlagChanged();
    partial void OnCreatedByChanging(System.Nullable<int> value);
    partial void OnCreatedByChanged();
    partial void OnCreatedAtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreatedAtChanged();
    #endregion
	
	public TB_Document()
	{
		this._TB_Company = default(EntityRef<TB_Company>);
		this._TB_DocumentCatagory = default(EntityRef<TB_DocumentCatagory>);
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int ID
	{
		get
		{
			return this._ID;
		}
		set
		{
			if ((this._ID != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._ID = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UID", DbType="UniqueIdentifier")]
	public System.Nullable<System.Guid> UID
	{
		get
		{
			return this._UID;
		}
		set
		{
			if ((this._UID != value))
			{
				this.OnUIDChanging(value);
				this.SendPropertyChanging();
				this._UID = value;
				this.SendPropertyChanged("UID");
				this.OnUIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDCompany", DbType="Int NOT NULL")]
	public int IDCompany
	{
		get
		{
			return this._IDCompany;
		}
		set
		{
			if ((this._IDCompany != value))
			{
				if (this._TB_Company.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnIDCompanyChanging(value);
				this.SendPropertyChanging();
				this._IDCompany = value;
				this.SendPropertyChanged("IDCompany");
				this.OnIDCompanyChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDCategory", DbType="Int NOT NULL")]
	public int IDCategory
	{
		get
		{
			return this._IDCategory;
		}
		set
		{
			if ((this._IDCategory != value))
			{
				if (this._TB_DocumentCatagory.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnIDCategoryChanging(value);
				this.SendPropertyChanging();
				this._IDCategory = value;
				this.SendPropertyChanged("IDCategory");
				this.OnIDCategoryChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="VarChar(255) NOT NULL", CanBeNull=false)]
	public string Name
	{
		get
		{
			return this._Name;
		}
		set
		{
			if ((this._Name != value))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._Name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Description", DbType="Text", UpdateCheck=UpdateCheck.Never)]
	public string Description
	{
		get
		{
			return this._Description;
		}
		set
		{
			if ((this._Description != value))
			{
				this.OnDescriptionChanging(value);
				this.SendPropertyChanging();
				this._Description = value;
				this.SendPropertyChanged("Description");
				this.OnDescriptionChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Flag", DbType="Int")]
	public System.Nullable<int> Flag
	{
		get
		{
			return this._Flag;
		}
		set
		{
			if ((this._Flag != value))
			{
				this.OnFlagChanging(value);
				this.SendPropertyChanging();
				this._Flag = value;
				this.SendPropertyChanged("Flag");
				this.OnFlagChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="Int")]
	public System.Nullable<int> CreatedBy
	{
		get
		{
			return this._CreatedBy;
		}
		set
		{
			if ((this._CreatedBy != value))
			{
				this.OnCreatedByChanging(value);
				this.SendPropertyChanging();
				this._CreatedBy = value;
				this.SendPropertyChanged("CreatedBy");
				this.OnCreatedByChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedAt", DbType="DateTime")]
	public System.Nullable<System.DateTime> CreatedAt
	{
		get
		{
			return this._CreatedAt;
		}
		set
		{
			if ((this._CreatedAt != value))
			{
				this.OnCreatedAtChanging(value);
				this.SendPropertyChanging();
				this._CreatedAt = value;
				this.SendPropertyChanged("CreatedAt");
				this.OnCreatedAtChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TB_Company_TB_Document", Storage="_TB_Company", ThisKey="IDCompany", OtherKey="ID", IsForeignKey=true)]
	public TB_Company TB_Company
	{
		get
		{
			return this._TB_Company.Entity;
		}
		set
		{
			TB_Company previousValue = this._TB_Company.Entity;
			if (((previousValue != value) 
						|| (this._TB_Company.HasLoadedOrAssignedValue == false)))
			{
				this.SendPropertyChanging();
				if ((previousValue != null))
				{
					this._TB_Company.Entity = null;
					previousValue.TB_Documents.Remove(this);
				}
				this._TB_Company.Entity = value;
				if ((value != null))
				{
					value.TB_Documents.Add(this);
					this._IDCompany = value.ID;
				}
				else
				{
					this._IDCompany = default(int);
				}
				this.SendPropertyChanged("TB_Company");
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TB_DocumentCatagory_TB_Document", Storage="_TB_DocumentCatagory", ThisKey="IDCategory", OtherKey="ID", IsForeignKey=true)]
	public TB_DocumentCatagory TB_DocumentCatagory
	{
		get
		{
			return this._TB_DocumentCatagory.Entity;
		}
		set
		{
			TB_DocumentCatagory previousValue = this._TB_DocumentCatagory.Entity;
			if (((previousValue != value) 
						|| (this._TB_DocumentCatagory.HasLoadedOrAssignedValue == false)))
			{
				this.SendPropertyChanging();
				if ((previousValue != null))
				{
					this._TB_DocumentCatagory.Entity = null;
					previousValue.TB_Documents.Remove(this);
				}
				this._TB_DocumentCatagory.Entity = value;
				if ((value != null))
				{
					value.TB_Documents.Add(this);
					this._IDCategory = value.ID;
				}
				else
				{
					this._IDCategory = default(int);
				}
				this.SendPropertyChanged("TB_DocumentCatagory");
			}
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_DocumentCatagory")]
public partial class TB_DocumentCatagory : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _ID;
	
	private System.Nullable<System.Guid> _UID;
	
	private string _Name;
	
	private System.Nullable<int> _CreatedBy;
	
	private System.Nullable<System.DateTime> _CreatedAt;
	
	private EntitySet<TB_Document> _TB_Documents;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnUIDChanging(System.Nullable<System.Guid> value);
    partial void OnUIDChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnCreatedByChanging(System.Nullable<int> value);
    partial void OnCreatedByChanged();
    partial void OnCreatedAtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreatedAtChanged();
    #endregion
	
	public TB_DocumentCatagory()
	{
		this._TB_Documents = new EntitySet<TB_Document>(new Action<TB_Document>(this.attach_TB_Documents), new Action<TB_Document>(this.detach_TB_Documents));
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int ID
	{
		get
		{
			return this._ID;
		}
		set
		{
			if ((this._ID != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._ID = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UID", DbType="UniqueIdentifier")]
	public System.Nullable<System.Guid> UID
	{
		get
		{
			return this._UID;
		}
		set
		{
			if ((this._UID != value))
			{
				this.OnUIDChanging(value);
				this.SendPropertyChanging();
				this._UID = value;
				this.SendPropertyChanged("UID");
				this.OnUIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="VarChar(255) NOT NULL", CanBeNull=false)]
	public string Name
	{
		get
		{
			return this._Name;
		}
		set
		{
			if ((this._Name != value))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._Name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="Int")]
	public System.Nullable<int> CreatedBy
	{
		get
		{
			return this._CreatedBy;
		}
		set
		{
			if ((this._CreatedBy != value))
			{
				this.OnCreatedByChanging(value);
				this.SendPropertyChanging();
				this._CreatedBy = value;
				this.SendPropertyChanged("CreatedBy");
				this.OnCreatedByChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedAt", DbType="DateTime")]
	public System.Nullable<System.DateTime> CreatedAt
	{
		get
		{
			return this._CreatedAt;
		}
		set
		{
			if ((this._CreatedAt != value))
			{
				this.OnCreatedAtChanging(value);
				this.SendPropertyChanging();
				this._CreatedAt = value;
				this.SendPropertyChanged("CreatedAt");
				this.OnCreatedAtChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TB_DocumentCatagory_TB_Document", Storage="_TB_Documents", ThisKey="ID", OtherKey="IDCategory")]
	public EntitySet<TB_Document> TB_Documents
	{
		get
		{
			return this._TB_Documents;
		}
		set
		{
			this._TB_Documents.Assign(value);
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	
	private void attach_TB_Documents(TB_Document entity)
	{
		this.SendPropertyChanging();
		entity.TB_DocumentCatagory = this;
	}
	
	private void detach_TB_Documents(TB_Document entity)
	{
		this.SendPropertyChanging();
		entity.TB_DocumentCatagory = null;
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_Position")]
public partial class TB_Position : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _ID;
	
	private System.Nullable<System.Guid> _UID;
	
	private string _Name;
	
	private System.Nullable<int> _CreatedBy;
	
	private System.Nullable<System.DateTime> _CreatedAt;
	
	private EntitySet<TBUser> _TBUsers;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnUIDChanging(System.Nullable<System.Guid> value);
    partial void OnUIDChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnCreatedByChanging(System.Nullable<int> value);
    partial void OnCreatedByChanged();
    partial void OnCreatedAtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreatedAtChanged();
    #endregion
	
	public TB_Position()
	{
		this._TBUsers = new EntitySet<TBUser>(new Action<TBUser>(this.attach_TBUsers), new Action<TBUser>(this.detach_TBUsers));
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int ID
	{
		get
		{
			return this._ID;
		}
		set
		{
			if ((this._ID != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._ID = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UID", DbType="UniqueIdentifier")]
	public System.Nullable<System.Guid> UID
	{
		get
		{
			return this._UID;
		}
		set
		{
			if ((this._UID != value))
			{
				this.OnUIDChanging(value);
				this.SendPropertyChanging();
				this._UID = value;
				this.SendPropertyChanged("UID");
				this.OnUIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="VarChar(255) NOT NULL", CanBeNull=false)]
	public string Name
	{
		get
		{
			return this._Name;
		}
		set
		{
			if ((this._Name != value))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._Name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="Int")]
	public System.Nullable<int> CreatedBy
	{
		get
		{
			return this._CreatedBy;
		}
		set
		{
			if ((this._CreatedBy != value))
			{
				this.OnCreatedByChanging(value);
				this.SendPropertyChanging();
				this._CreatedBy = value;
				this.SendPropertyChanged("CreatedBy");
				this.OnCreatedByChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedAt", DbType="DateTime")]
	public System.Nullable<System.DateTime> CreatedAt
	{
		get
		{
			return this._CreatedAt;
		}
		set
		{
			if ((this._CreatedAt != value))
			{
				this.OnCreatedAtChanging(value);
				this.SendPropertyChanging();
				this._CreatedAt = value;
				this.SendPropertyChanged("CreatedAt");
				this.OnCreatedAtChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TB_Position_TBUser", Storage="_TBUsers", ThisKey="ID", OtherKey="IDPosition")]
	public EntitySet<TBUser> TBUsers
	{
		get
		{
			return this._TBUsers;
		}
		set
		{
			this._TBUsers.Assign(value);
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	
	private void attach_TBUsers(TBUser entity)
	{
		this.SendPropertyChanging();
		entity.TB_Position = this;
	}
	
	private void detach_TBUsers(TBUser entity)
	{
		this.SendPropertyChanging();
		entity.TB_Position = null;
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TBUser")]
public partial class TBUser : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _ID;
	
	private System.Nullable<System.Guid> _UID;
	
	private int _IDCompany;
	
	private int _IDPosition;
	
	private string _Name;
	
	private string _Address;
	
	private string _Email;
	
	private string _Telephone;
	
	private string _Username;
	
	private string _Password;
	
	private string _Role;
	
	private System.Nullable<int> _Flag;
	
	private System.Nullable<int> _CreatedBy;
	
	private System.Nullable<System.DateTime> _CreatedAt;
	
	private EntityRef<TB_Company> _TB_Company;
	
	private EntityRef<TB_Position> _TB_Position;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnUIDChanging(System.Nullable<System.Guid> value);
    partial void OnUIDChanged();
    partial void OnIDCompanyChanging(int value);
    partial void OnIDCompanyChanged();
    partial void OnIDPositionChanging(int value);
    partial void OnIDPositionChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnAddressChanging(string value);
    partial void OnAddressChanged();
    partial void OnEmailChanging(string value);
    partial void OnEmailChanged();
    partial void OnTelephoneChanging(string value);
    partial void OnTelephoneChanged();
    partial void OnUsernameChanging(string value);
    partial void OnUsernameChanged();
    partial void OnPasswordChanging(string value);
    partial void OnPasswordChanged();
    partial void OnRoleChanging(string value);
    partial void OnRoleChanged();
    partial void OnFlagChanging(System.Nullable<int> value);
    partial void OnFlagChanged();
    partial void OnCreatedByChanging(System.Nullable<int> value);
    partial void OnCreatedByChanged();
    partial void OnCreatedAtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreatedAtChanged();
    #endregion
	
	public TBUser()
	{
		this._TB_Company = default(EntityRef<TB_Company>);
		this._TB_Position = default(EntityRef<TB_Position>);
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int ID
	{
		get
		{
			return this._ID;
		}
		set
		{
			if ((this._ID != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._ID = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UID", DbType="UniqueIdentifier")]
	public System.Nullable<System.Guid> UID
	{
		get
		{
			return this._UID;
		}
		set
		{
			if ((this._UID != value))
			{
				this.OnUIDChanging(value);
				this.SendPropertyChanging();
				this._UID = value;
				this.SendPropertyChanged("UID");
				this.OnUIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDCompany", DbType="Int NOT NULL")]
	public int IDCompany
	{
		get
		{
			return this._IDCompany;
		}
		set
		{
			if ((this._IDCompany != value))
			{
				if (this._TB_Company.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnIDCompanyChanging(value);
				this.SendPropertyChanging();
				this._IDCompany = value;
				this.SendPropertyChanged("IDCompany");
				this.OnIDCompanyChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IDPosition", DbType="Int NOT NULL")]
	public int IDPosition
	{
		get
		{
			return this._IDPosition;
		}
		set
		{
			if ((this._IDPosition != value))
			{
				if (this._TB_Position.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OnIDPositionChanging(value);
				this.SendPropertyChanging();
				this._IDPosition = value;
				this.SendPropertyChanged("IDPosition");
				this.OnIDPositionChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="VarChar(255) NOT NULL", CanBeNull=false)]
	public string Name
	{
		get
		{
			return this._Name;
		}
		set
		{
			if ((this._Name != value))
			{
				this.OnNameChanging(value);
				this.SendPropertyChanging();
				this._Name = value;
				this.SendPropertyChanged("Name");
				this.OnNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Address", DbType="Text", UpdateCheck=UpdateCheck.Never)]
	public string Address
	{
		get
		{
			return this._Address;
		}
		set
		{
			if ((this._Address != value))
			{
				this.OnAddressChanging(value);
				this.SendPropertyChanging();
				this._Address = value;
				this.SendPropertyChanged("Address");
				this.OnAddressChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Email", DbType="VarChar(50)")]
	public string Email
	{
		get
		{
			return this._Email;
		}
		set
		{
			if ((this._Email != value))
			{
				this.OnEmailChanging(value);
				this.SendPropertyChanging();
				this._Email = value;
				this.SendPropertyChanged("Email");
				this.OnEmailChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Telephone", DbType="VarChar(14)")]
	public string Telephone
	{
		get
		{
			return this._Telephone;
		}
		set
		{
			if ((this._Telephone != value))
			{
				this.OnTelephoneChanging(value);
				this.SendPropertyChanging();
				this._Telephone = value;
				this.SendPropertyChanged("Telephone");
				this.OnTelephoneChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Username", DbType="VarChar(50)")]
	public string Username
	{
		get
		{
			return this._Username;
		}
		set
		{
			if ((this._Username != value))
			{
				this.OnUsernameChanging(value);
				this.SendPropertyChanging();
				this._Username = value;
				this.SendPropertyChanged("Username");
				this.OnUsernameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Password", DbType="VarChar(255)")]
	public string Password
	{
		get
		{
			return this._Password;
		}
		set
		{
			if ((this._Password != value))
			{
				this.OnPasswordChanging(value);
				this.SendPropertyChanging();
				this._Password = value;
				this.SendPropertyChanged("Password");
				this.OnPasswordChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Role", DbType="VarChar(50)")]
	public string Role
	{
		get
		{
			return this._Role;
		}
		set
		{
			if ((this._Role != value))
			{
				this.OnRoleChanging(value);
				this.SendPropertyChanging();
				this._Role = value;
				this.SendPropertyChanged("Role");
				this.OnRoleChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Flag", DbType="Int")]
	public System.Nullable<int> Flag
	{
		get
		{
			return this._Flag;
		}
		set
		{
			if ((this._Flag != value))
			{
				this.OnFlagChanging(value);
				this.SendPropertyChanging();
				this._Flag = value;
				this.SendPropertyChanged("Flag");
				this.OnFlagChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="Int")]
	public System.Nullable<int> CreatedBy
	{
		get
		{
			return this._CreatedBy;
		}
		set
		{
			if ((this._CreatedBy != value))
			{
				this.OnCreatedByChanging(value);
				this.SendPropertyChanging();
				this._CreatedBy = value;
				this.SendPropertyChanged("CreatedBy");
				this.OnCreatedByChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedAt", DbType="DateTime")]
	public System.Nullable<System.DateTime> CreatedAt
	{
		get
		{
			return this._CreatedAt;
		}
		set
		{
			if ((this._CreatedAt != value))
			{
				this.OnCreatedAtChanging(value);
				this.SendPropertyChanging();
				this._CreatedAt = value;
				this.SendPropertyChanged("CreatedAt");
				this.OnCreatedAtChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TB_Company_TBUser", Storage="_TB_Company", ThisKey="IDCompany", OtherKey="ID", IsForeignKey=true)]
	public TB_Company TB_Company
	{
		get
		{
			return this._TB_Company.Entity;
		}
		set
		{
			TB_Company previousValue = this._TB_Company.Entity;
			if (((previousValue != value) 
						|| (this._TB_Company.HasLoadedOrAssignedValue == false)))
			{
				this.SendPropertyChanging();
				if ((previousValue != null))
				{
					this._TB_Company.Entity = null;
					previousValue.TBUsers.Remove(this);
				}
				this._TB_Company.Entity = value;
				if ((value != null))
				{
					value.TBUsers.Add(this);
					this._IDCompany = value.ID;
				}
				else
				{
					this._IDCompany = default(int);
				}
				this.SendPropertyChanged("TB_Company");
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TB_Position_TBUser", Storage="_TB_Position", ThisKey="IDPosition", OtherKey="ID", IsForeignKey=true)]
	public TB_Position TB_Position
	{
		get
		{
			return this._TB_Position.Entity;
		}
		set
		{
			TB_Position previousValue = this._TB_Position.Entity;
			if (((previousValue != value) 
						|| (this._TB_Position.HasLoadedOrAssignedValue == false)))
			{
				this.SendPropertyChanging();
				if ((previousValue != null))
				{
					this._TB_Position.Entity = null;
					previousValue.TBUsers.Remove(this);
				}
				this._TB_Position.Entity = value;
				if ((value != null))
				{
					value.TBUsers.Add(this);
					this._IDPosition = value.ID;
				}
				else
				{
					this._IDPosition = default(int);
				}
				this.SendPropertyChanged("TB_Position");
			}
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
#pragma warning restore 1591
