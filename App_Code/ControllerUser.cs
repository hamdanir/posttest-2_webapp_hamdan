﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ControllerUser
/// </summary>
public class ControllerUser : ClassBase
{
    public ControllerUser(DataClassesDatabaseDataContext _db) : base (_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //menampilkan data
    public void ViewData()
    {
        db.TBUsers.Select(x => new
        {
            x.ID,
            x.IDCompany,
            x.IDPosition,
            NameCompany = x.TB_Company.Name,
            NamePosition = x.TB_Position.Name,
            x.Name,
            x.Address,
            x.Email,
            x.Telephone,
            x.Username,
            x.Role,
            x.Flag,
            x.CreatedBy
        }).ToArray();
    }

    //Create data
    public TBUser Create(int IDCompany, int IDPosition, string name, string address, string email, string telephone, string username, string password, string role)
    {
        TBUser user = new TBUser
        {
            UID = Guid.NewGuid(),
            IDCompany = IDCompany,
            IDPosition = IDPosition,
            Name = name,
            Address = address,
            Email = email,
            Telephone = telephone,
            Username = username,
            Password = password,
            Role = role,
            Flag = 1,
            CreatedBy = 1,
            CreatedAt = DateTime.Now


        };

        db.TBUsers.InsertOnSubmit(user);
        return user;
    }

    public TBUser Cari(int ID)
    {
        return db.TBUsers.FirstOrDefault(x => x.ID == ID);
    }


    //Udatae
    public TBUser Update(int ID, int IDCompany, int IDPosition, string name, string address, string email, string telephone, string username, string password, string role)
    {
        var user = Cari(ID);

        if (user != null)
        {
            user.IDCompany = IDCompany;
            user.IDPosition = IDPosition;
            user.Name = name;
            user.Address = address;
            user.Email = email;
            user.Telephone = telephone;
            user.Username = username;
            user.Password = password;
            user.Role = role;
            user.Flag = 1;
            user.CreatedBy = 1;
            

            return user;
        }
        else
        {
            return null;
        }
    }


    //Delete
    public TBUser Delete(int ID)
    {
        var user = Cari(ID);

        if (user != null)
        {
            db.TBUsers.DeleteOnSubmit(user);
            db.SubmitChanges();
            return user;
        }
        else return null;
    }

    public TBUser CreateUpdate(int ID, string Nama)
    {
        var user = Cari(ID);

        if (user == null)
        {
            user = new TBUser
            {
                UID = Guid.NewGuid(),
                Name = Nama
            };

            db.TBUsers.InsertOnSubmit(user);
        }
        else
        {
            user.Name = Nama;
        }

        return user;
    }


}