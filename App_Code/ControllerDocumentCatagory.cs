﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerDocumentCategory
/// </summary>
public class ControllerDocumentCatagory : ClassBase
{
    public ControllerDocumentCatagory(DataClassesDatabaseDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public TB_DocumentCatagory[] Data()
    {
        return db.TB_DocumentCatagories.ToArray();
    }

    //Create data
    public TB_DocumentCatagory Create(string name)
    {
        TB_DocumentCatagory documentCatagory = new TB_DocumentCatagory
        {
            Name = name,
            CreatedBy = 1,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now,
        };

        db.TB_DocumentCatagories.InsertOnSubmit(documentCatagory);
        return documentCatagory;
    }

    public TB_DocumentCatagory Cari(string UID)
    {
        return db.TB_DocumentCatagories.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    public TB_DocumentCatagory Cari(Guid UID)
    {
        return db.TB_DocumentCatagories.FirstOrDefault(item => item.UID == UID);
    }

    //Udatae
    public TB_DocumentCatagory Update(string UID, string name)
    {
        var documentCatagory = Cari(UID);

        if (documentCatagory != null)
        {
            documentCatagory.Name = name;
            documentCatagory.CreatedBy = 1;

            return documentCatagory;
        }
        else
        {
            return null;
        }
    }


    //Delete
    public TB_DocumentCatagory Delete(string UID)
    {
        var documentCatagory = Cari(UID);

        if (documentCatagory != null)
        {
            db.TB_DocumentCatagories.DeleteOnSubmit(documentCatagory);
            db.SubmitChanges();
            return documentCatagory;
        }
        else return null;
    }

    public TB_DocumentCatagory CreateUpdate(Guid UID, string Nama)
    {
        var documentCatagory = Cari(UID);

        if (documentCatagory == null)
        {
            documentCatagory = new TB_DocumentCatagory
            {
                UID = Guid.NewGuid(),
                Name = Nama
            };

            db.TB_DocumentCatagories.InsertOnSubmit(documentCatagory);
        }
        else
        {
            documentCatagory.Name = Nama;
        }

        return documentCatagory;
    }

    //Dropdown
    public void DropDownListDocumentCatagory(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> documentCatagory = new List<ListItem>();

        documentCatagory.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        documentCatagory.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name,
        }));

        return documentCatagory.ToArray();
    }
}