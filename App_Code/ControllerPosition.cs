﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerPosition
/// </summary>
public class ControllerPosition : ClassBase
{
    public ControllerPosition(DataClassesDatabaseDataContext _db) : base(_db)
    {

    }

    //menampilkan data
    public TB_Position[] Data()
    {
        return db.TB_Positions.ToArray();
    }

    //Create data
    public TB_Position Create(string name)
    {
        TB_Position position = new TB_Position
        {
            Name = name,
            CreatedBy = 1,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now,
        };

        db.TB_Positions.InsertOnSubmit(position);
        return position;
    }

    public TB_Position Cari(string UID)
    {
        return db.TB_Positions.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    public TB_Position Cari(Guid UID)
    {
        return db.TB_Positions.FirstOrDefault(item => item.UID == UID);
    }

    //Udatae
    public TB_Position Update(string UID, string name)
    {
        var position = Cari(UID);

        if (position != null)
        {
            position.Name = name;
            position.CreatedBy = 1;

            return position;
        }
        else
        {
            return null;
        }
    }


    //Delete
    public TB_Position Delete(string UID)
    {
        var position = Cari(UID);

        if (position != null)
        {
            db.TB_Positions.DeleteOnSubmit(position);
            db.SubmitChanges();
            return position;
        }
        else return null;
    }

    public TB_Position CreateUpdate(Guid UID, string Nama)
    {
        var position = Cari(UID);

        if (position == null)
        {
            position = new TB_Position
            {
                UID = Guid.NewGuid(),
                Name = Nama
            };

            db.TB_Positions.InsertOnSubmit(position);
        }
        else
        {
            position.Name = Nama;
        }

        return position;
    }

    //Dropdown
    public void DropDownListPosition(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> position = new List<ListItem>();

        position.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        position.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name,
        }));

        return position.ToArray();
    }
}

