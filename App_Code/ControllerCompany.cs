﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerCompany
/// </summary>
public class ControllerCompany : ClassBase
{
    public ControllerCompany(DataClassesDatabaseDataContext _db) : base(_db)
    {

    }

    //menampilkan data
    public TB_Company[] Data()
    {
        return db.TB_Companies.ToArray();
    }

    //Create data
    public TB_Company Create(string name, string address, string telephone, int flag, int createdby)
    {
        TB_Company company = new TB_Company
        {
            Name = name,
            Address = address,
            Telephone = telephone,
            FLag = flag,
            CreatedBy = 1,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now,
        };

        db.TB_Companies.InsertOnSubmit(company);
        return company;
    }

    public TB_Company Cari(string UID)
    {
        return db.TB_Companies.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    public TB_Company Cari(Guid UID)
    {
        return db.TB_Companies.FirstOrDefault(item => item.UID == UID);
    }

    //Udatae
    public TB_Company Update(string UID, string name, string address, string telephone, int flag, int createdby)
    {
        var company = Cari(UID);

        if (company != null)
        {
            company.Name = name;
            company.Address = address;
            company.Telephone = telephone;
            company.FLag = flag;
            company.CreatedBy = 1;

            return company;
        }
        else
        {
            return null;
        }
    }


    //Delete
    public TB_Company Delete(string UID)
    {
        var company = Cari(UID);

        if (company != null)
        {
            db.TB_Companies.DeleteOnSubmit(company);
            db.SubmitChanges();
            return company;
        }
        else return null;
    }

    public TB_Company CreateUpdate(Guid UID, string Nama)
    {
        var company = Cari(UID);

        if (company == null)
        {
            company = new TB_Company
            {
                UID = Guid.NewGuid(),
                Name = Nama
            };

            db.TB_Companies.InsertOnSubmit(company);
        }
        else
        {
            company.Name = Nama;
        }

        return company;
    }

    //Dropdown
    public void DropDownListCompany(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> company = new List<ListItem>();

        company.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        company.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name,
        }));

        return company.ToArray();
    }
}

