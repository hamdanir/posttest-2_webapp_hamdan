﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ControllerDocument
/// </summary>
public class ControllerDocument : ClassBase
{
    public ControllerDocument(DataClassesDatabaseDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //menampilkan data
    public void ViewData()
    {
        db.TB_Documents.Select(x => new
        {
            x.ID,
            x.IDCompany,
            x.IDCategory,
            NameCompany = x.TB_Company.Name,
            NameCategory = x.TB_DocumentCatagory.Name,
            x.Name,
            x.Description,
            x.Flag,
            x.CreatedBy
        }).ToArray();
    }

    //Create data
    public TB_Document Create(int IDCompany, int IDCategory, string name, string description)
    {
        TB_Document user = new TB_Document
        {
            UID = Guid.NewGuid(),
            IDCompany = IDCompany,
            IDCategory = IDCategory,
            Name = name,
            Description = description,
            Flag = 1,
            CreatedBy = 1,
            CreatedAt = DateTime.Now


        };

        db.TB_Documents.InsertOnSubmit(user);
        return user;
    }

    public TB_Document Cari(int ID)
    {
        return db.TB_Documents.FirstOrDefault(x => x.ID == ID);
    }


    //Udatae
    public TB_Document Update(int ID, int IDCompany, int IDCategory, string name, string description)
    {
        var document = Cari(ID);

        if (document != null)
        {
            document.IDCompany = IDCompany;
            document.IDCategory = IDCategory;
            document.Name = name;
            document.Description = description;
            document.Flag = 1;
            document.CreatedBy = 1;


            return document;
        }
        else
        {
            return null;
        }
    }


    //Delete
    public TB_Document Delete(int ID)
    {
        var document = Cari(ID);

        if (document != null)
        {
            db.TB_Documents.DeleteOnSubmit(document);
            db.SubmitChanges();
            return document;
        }
        else return null;
    }

    public TB_Document CreateUpdate(int ID, string Nama)
    {
        var document = Cari(ID);

        if (document == null)
        {
            document = new TB_Document
            {
                UID = Guid.NewGuid(),
                Name = Nama
            };

            db.TB_Documents.InsertOnSubmit(document);
        }
        else
        {
            document.Name = Nama;
        }

        return document;
    }
}