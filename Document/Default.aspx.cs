﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Document_Default : System.Web.UI.Page
{
    DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    public void LoadData()
    {

        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            repeaterDocument.DataSource = db.TB_Documents.Select(x => new
            {
                x.ID,
                x.UID,
                x.IDCompany,
                x.IDCategory,
                NameCompany = x.TB_Company.Name,
                NameCatagory = x.TB_DocumentCatagory.Name,
                x.Name,
                x.Description,
                x.Flag

            }).ToArray();
            repeaterDocument.DataBind();
        }
    }

    protected void repeaterDocument_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Document/Form.aspx?id=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var document = db.TB_Documents.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                db.TB_Documents.DeleteOnSubmit(document);
                db.SubmitChanges();

                LoadData();
            }
        }
    }

}