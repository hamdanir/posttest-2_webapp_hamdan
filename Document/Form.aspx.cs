﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class Document_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropdown();
            LoadDocumentCatagory();
            FormUpdate();
        }
    }

    public void LoadDropdown()
    {

        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            ControllerCompany controllerCompany = new ControllerCompany(db);
            ListItem[] companyItems = controllerCompany.DropDownList();
            listCompany.Items.AddRange(companyItems);

        }
    }

    public void LoadDocumentCatagory()
    {

        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            ControllerDocumentCatagory controllerDocumentCatagory = new ControllerDocumentCatagory(db);
            ListItem[] documentCatagoryItems = controllerDocumentCatagory.DropDownList();
            ListDocumentCatagory.Items.AddRange(documentCatagoryItems);

        }
    }

    public void FormUpdate()
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {

            ControllerDocument controllerDocument = new ControllerDocument(db);

            var document = controllerDocument.Cari(Convert.ToInt32(Request.QueryString["id"]));

            if (document != null)
            {
                listCompany.SelectedValue = document.IDCompany.ToString();
                ListDocumentCatagory.SelectedValue = document.IDCategory.ToString();
                InputName.Text = document.Name;
                InputDescription.Text = document.Description;




                btnOk.Text = "Update";
                LabelTitle.Text = "Update Document";
            }
            else
            {
                btnOk.Text = "Add New";
                LabelTitle.Text = "Add New Document";
            }

        }
    }



    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerDocument controllerDocument = new ControllerDocument(db);

                if (btnOk.Text == "Add New")
                {
                    controllerDocument.Create(int.Parse(listCompany.SelectedValue), int.Parse(ListDocumentCatagory.SelectedValue), InputName.Text, InputDescription.Text);
                }
                else if (btnOk.Text == "Update")
                    controllerDocument.Update(int.Parse(Request.QueryString["id"]), int.Parse(listCompany.SelectedValue), int.Parse(ListDocumentCatagory.SelectedValue), InputName.Text, InputDescription.Text);
                db.SubmitChanges();

                Response.Redirect("/Document/Default.aspx");
            }
        }
    }
}