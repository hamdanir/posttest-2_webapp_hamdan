﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Document_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
    List Document
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <a href="Form.aspx" class="btn btn-success btn-sm">Add New</a>
    <br />
    <br />
    <div class="row">
      <div class="col-md-12">
          <div class="table-responsive">
              <table class="table table-condensed table-hover table-bordered">
                  <thead>
                      <tr class="active">
                          <th>No</th>
                          <th>Company</th>
                          <th>Catagory</th>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <asp:Repeater ID="repeaterDocument" runat="server" OnItemCommand="repeaterDocument_ItemCommand">
                          <ItemTemplate>
                              <tr>
                                  <td class="fitSize"><%# Container.ItemIndex + 1 %></td>
                                  <td><%# Eval("NameCompany") %></td>
                                  <td><%# Eval("NameCatagory") %></td>
                                  <td><%# Eval("Name") %></td>
                                  <td><%# Eval("Description") %></td>
                                  <td class="text-right fitSize">
                                      <asp:Button ID="btnUpdate" CssClass="btn btn-info btn-sm" runat="server" Text="Update"  CommandName="Update" CommandArgument='<%# Eval("ID") %>'   />
                                      <asp:Button ID="btnDelete" CssClass="btn btn-danger btn-sm" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("UID") %>' OnClienClik='<%# "return confirm(\"Apakah Anda Yakin menghapus data" + Eval("Name") +"\")" %>' />
                                  </td>
                              </tr>
                          </ItemTemplate>

                      </asp:Repeater>
                  </tbody>
              </table>
          </div>
      </div>
        
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" Runat="Server">
</asp:Content>

