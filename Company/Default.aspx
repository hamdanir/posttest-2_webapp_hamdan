﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Company_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
    List Company
    <hr />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <a href="Form.aspx" class="btn btn-success btn-sm">Add New</a>
    <br />
    <br />
    <div class="row">
      <div class="col-md-12">
          <div class="table-responsive">
              <table class="table table-condensed table-hover table-bordered">
                  <thead>
                      <tr class="active">
                          <th>No</th>
                          <th>Nama</th>
                          <th>Address</th>
                          <th>Telepohne</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <asp:Repeater ID="repeaterCompany" runat="server" OnItemCommand="repeaterCompany_ItemCommand">
                          <ItemTemplate>
                              <tr>
                                  <td class="fitSize"><%# Container.ItemIndex + 1 %></td>
                                  <td><%# Eval("Name") %></td>
                                  <td><%# Eval("Address") %></td>
                                  <td><%# Eval("Telephone") %></td>
                                  <td class="text-right fitSize">
                                      <asp:Button ID="ButtonUbah" CssClass="btn btn-info btn-sm" runat="server" Text="Update"  CommandName="Update" CommandArgument='<%# Eval("UID") %>' />
                                      <asp:Button ID="ButtonHapus" CssClass="btn btn-danger btn-sm" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("UID") %>' OnClienClik='<%# "return confirm(\"Apakah Anda Yakin menghapus data" + Eval("Name") +"\")" %>'/>
                                  </td>
                              </tr>
                          </ItemTemplate>

                      </asp:Repeater>
                  </tbody>
              </table>
          </div>
      </div>
        
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" Runat="Server">
</asp:Content>

