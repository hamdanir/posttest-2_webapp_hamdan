﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_Default : System.Web.UI.Page
{
    DataClassesDatabaseDataContext  db = new DataClassesDatabaseDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                LoadData(db);
            }
        }
    }

    public void LoadData(DataClassesDatabaseDataContext db)
    {
        ControllerCompany controllerCompany = new ControllerCompany(db);

        repeaterCompany.DataSource = controllerCompany.Data();
        repeaterCompany.DataBind();
    }

    protected  void repeaterCompany_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Company/Form.aspx?uid=" + e.CommandArgument  );
            }
            else if (e.CommandName == "Delete")
            {
                var company = db.TB_Companies.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                db.TB_Companies.DeleteOnSubmit(company);
                db.SubmitChanges();

                LoadData(db);   
            }
        }
    }
}