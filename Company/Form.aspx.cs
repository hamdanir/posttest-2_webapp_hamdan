﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using(DataClassesDatabaseDataContext db =  new DataClassesDatabaseDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);

                var company = controllerCompany.Cari(Request.QueryString["uid"]);

                if (company != null) 
                {
                    InputName.Text = company.Name;
                    InputAddress.Text = company.Address;
                    InputTelephone.Text = company.Telephone;

                    ButtonOK.Text = "Update";
                    LabelTitle.Text = "Update Company";
                }
                else
                {
                    ButtonOK.Text = "Add New";
                    LabelTitle.Text = "Add New Company";
                }
            }
        }
    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);

                if (ButtonOK.Text == "Add New")
                    controllerCompany.Create(InputName.Text, InputAddress.Text, InputTelephone.Text);
                else if (ButtonOK.Text == "Update")
                    controllerCompany.Update(Request.QueryString["uid"], InputName.Text, InputAddress.Text, InputTelephone.Text);

                db.SubmitChanges();

                Response.Redirect("./Default.aspx");
            }

        }
    }

    protected void ButtonKeluar_Click(object sender, EventArgs e)
    {
        Response.Redirect("./Default.aspx");
    }
}