﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Position_Default : System.Web.UI.Page
{

    DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                LoadData(db);
            }
        }
    }

    public void LoadData(DataClassesDatabaseDataContext db)
    {
        ControllerPosition position = new ControllerPosition(db);
        repeaterPosition.DataSource = position.Data();
        repeaterPosition.DataBind();
    }



    protected void repeaterPosition_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Position/Form.aspx?uid=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var position = db.TB_Positions.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                db.TB_Positions.DeleteOnSubmit(position);
                db.SubmitChanges();

                LoadData(db);
            }
        }
    }

    
}