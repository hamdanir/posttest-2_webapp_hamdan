﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Position_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using(DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerPosition controllerPosition = new ControllerPosition(db);

                var position = controllerPosition.Cari(Request.QueryString["uid"]);

                if (position != null)
                {
                    InputPosition.Text = position.Name;

                    ButtonOK.Text = "Update";
                    LabelTitle.Text = "Update position";
                }
                else
                {
                    ButtonOK.Text = "Add New";
                    LabelTitle.Text = "Add New position";
                }
            }
        }
    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerPosition controllerPosition = new ControllerPosition(db);

                if (ButtonOK.Text == "Add New")
                    controllerPosition.Create(InputPosition.Text);
                else if (ButtonOK.Text == "Update")
                    controllerPosition.Update(Request.QueryString["uid"], InputPosition.Text);

                db.SubmitChanges();

                Response.Redirect("./Default.aspx");
            }

        }
    }

    protected void ButtonKeluar_Click(object sender, EventArgs e)
    {
        Response.Redirect("./Default.aspx");
    }
}