﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Default : System.Web.UI.Page
{
    DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    public void LoadData()
    {
        
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            repeaterUser.DataSource = db.TBUsers.Select(x => new
            {
                x.ID,
                x.UID,
                x.IDCompany,
                x.IDPosition,
                NameCompany = x.TB_Company.Name,
                NamePosition = x.TB_Position.Name,
                x.Name,
                x.Address,
                x.Email,
                x.Telephone,
                x.Username,
                x.Password,
                x.Role,
                x.Flag

            }).ToArray();
            repeaterUser.DataBind();
        }
    }
    
   

    protected void repeaterUser_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/User/Form.aspx?id=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var user = db.TBUsers.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                db.TBUsers.DeleteOnSubmit(user);
                db.SubmitChanges();

                LoadData();
            }
        }
    }
}