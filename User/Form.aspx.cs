﻿using ImageResizer.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class User_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropdown();
            LoadPosition();
            FormUpdate();
        }
    }

    public void LoadDropdown()
    {

        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            ControllerCompany controllerCompany = new ControllerCompany(db);
            ListItem[] companyItems = controllerCompany.DropDownList();
            listCompany.Items.AddRange(companyItems);

        }
    }

    public void LoadPosition()
    {

        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            ControllerPosition controllerPosition = new ControllerPosition(db);
            ListItem[] positionItems = controllerPosition.DropDownList();
            ListPosition.Items.AddRange(positionItems);

        }
    }

    public void FormUpdate()
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            
            ControllerUser controllerUser = new ControllerUser(db);

            var user = controllerUser.Cari(Convert.ToInt32(Request.QueryString["id"]));

            if (user != null)
            {
                listCompany.SelectedValue = user.IDCompany.ToString();
                ListPosition.SelectedValue = user.IDPosition.ToString();
                InputName.Text = user.Name;
                InputAddress.Text = user.Address;
                InputEmail.Text = user.Email;
                InputTelephone.Text = user.Telephone;
                InputUsername.Text = user.Username;
                InputPassword.Text = user.Password;
                InputRole.Text = user.Role;




                btnOk.Text = "Update";
                LabelTitle.Text = "Update User";
            }
            else
            {
                btnOk.Text = "Add New";
                LabelTitle.Text = "Add New User";
            }

        }
    }


 
    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerUser controllerUser = new ControllerUser(db);

                if (btnOk.Text == "Add New")
                {
                    controllerUser.Create(int.Parse(listCompany.SelectedValue), int.Parse(ListPosition.SelectedValue), InputName.Text, InputAddress.Text, InputEmail.Text, InputTelephone.Text, InputUsername.Text, InputPassword.Text, InputRole.Text);
                }
                else if (btnOk.Text == "Update")
                    controllerUser.Update(int.Parse(Request.QueryString["id"]), int.Parse(listCompany.SelectedValue), int.Parse(ListPosition.SelectedValue), InputName.Text, InputAddress.Text, InputEmail.Text, InputTelephone.Text, InputUsername.Text, InputPassword.Text, InputRole.Text);
                db.SubmitChanges();

                Response.Redirect("/User/Default.aspx");
            }
        }
    }
}