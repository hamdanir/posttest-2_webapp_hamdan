﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Form.aspx.cs" Inherits="User_Form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
    <asp:Label ID="LabelTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group mb-3">
                <div class="row ">
                    <div class="col-md-6">
                        <label class="fw-bold">Company</label>
                        <asp:DropDownList ID="listCompany" CssClass="form-control" runat="server" Width="100%"></asp:DropDownList>
                    </div>
                    <div class="col-md-6">
                        <label class="fw-bold">Position</label>
                        <asp:DropDownList ID="ListPosition" CssClass="form-control" runat="server" Width="100%"></asp:DropDownList>
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                        <label class="fw-bold">Name</label>
                        <asp:TextBox ID="InputName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
            </div>
            <div class="form-group mb-3">
                <div class="row">
                    <div class="col-md-6">
                        <label class="fw-bold">Email</label>
                        <asp:TextBox ID="InputEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                        <label class="fw-bold">Telephone</label>
                        <asp:TextBox ID="InputTelephone" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="row">
                    <div class="col-md-6">
                        <label class="fw-bold">Username</label>
                        <asp:TextBox ID="InputUsername" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                        <label class="fw-bold">Password</label>
                        <asp:TextBox ID="InputPassword" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="row">
                    <div class="col-md-6">
                        <label class="fw-bold">Role</label>
                        <asp:TextBox ID="InputRole" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    
                </div>
            </div>
            <div class="form-group mb-3">
                <label class="fw-bold">Address</label>
                <asp:TextBox ID="InputAddress" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>
            <asp:Button ID="btnOk" CssClass="btn btn-success btn-sm" runat="server" Text="Add New" OnClick="btnOk_Click"/>
            <a href="Default.aspx" class="btn btn-danger btn-sm">Cancel</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" Runat="Server">
</asp:Content>

