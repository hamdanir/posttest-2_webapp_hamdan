﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Dashboard_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
    DashBoard
    <hr />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <a href="../Company/Default.aspx" class="btn btn-success btn-sm">Company</a>
    <a href="../Position/Default.aspx" class="btn btn-success btn-sm">Position</a>
    <a href="../Document/Default.aspx" class="btn btn-success btn-sm">Document</a>
    <a href="../DocumentCatagory/Default.aspx" class="btn btn-success btn-sm">Document Category</a>
    <a href="../User/Default.aspx" class="btn btn-success btn-sm">User</a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" Runat="Server">
</asp:Content>

