USE [master]
GO
/****** Object:  Database [PreTestHamdan]    Script Date: 04/08/2023 11:16:32 ******/
CREATE DATABASE [PreTestHamdan] ON  PRIMARY 
( NAME = N'PreTestHamdan', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PreTestHamdan.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PreTestHamdan_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PreTestHamdan_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PreTestHamdan].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PreTestHamdan] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PreTestHamdan] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PreTestHamdan] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PreTestHamdan] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PreTestHamdan] SET ARITHABORT OFF 
GO
ALTER DATABASE [PreTestHamdan] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PreTestHamdan] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PreTestHamdan] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PreTestHamdan] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PreTestHamdan] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PreTestHamdan] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PreTestHamdan] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PreTestHamdan] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PreTestHamdan] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PreTestHamdan] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PreTestHamdan] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PreTestHamdan] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PreTestHamdan] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PreTestHamdan] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PreTestHamdan] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PreTestHamdan] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PreTestHamdan] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PreTestHamdan] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PreTestHamdan] SET  MULTI_USER 
GO
ALTER DATABASE [PreTestHamdan] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PreTestHamdan] SET DB_CHAINING OFF 
GO
USE [PreTestHamdan]
GO
/****** Object:  Table [dbo].[TB_Company]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Company](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NOT NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[FLag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TB_Company] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_Document]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Document](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NOT NULL,
	[IDCategory] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Description] [text] NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TB_Document] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_DocumentCatagory]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_DocumentCatagory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TB_DocumentCatagory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_Position]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Position](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TB_Position] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBUser]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NOT NULL,
	[IDPosition] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](255) NULL,
	[Role] [varchar](50) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[View_Document]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Document]
AS
SELECT        dbo.TB_Document.IDCompany, dbo.TB_Document.IDCategory, dbo.TB_Document.Name, dbo.TB_Document.Description, dbo.TB_Document.Flag, dbo.TB_Document.CreatedBy, dbo.TB_Document.CreatedAt, 
                         dbo.TB_DocumentCatagory.Name AS CategoryName, dbo.TB_Company.Name AS CompanyName
FROM            dbo.TB_Document INNER JOIN
                         dbo.TB_DocumentCatagory ON dbo.TB_Document.IDCategory = dbo.TB_DocumentCatagory.ID INNER JOIN
                         dbo.TB_Company ON dbo.TB_Document.IDCompany = dbo.TB_Company.ID
GO
/****** Object:  View [dbo].[View_User]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_User]
AS
SELECT        dbo.TB_Company.Name AS CompanyName, dbo.TB_Position.Name AS PositionName, dbo.TBUser.Name, dbo.TBUser.Address, dbo.TBUser.Email, dbo.TBUser.Telephone, dbo.TBUser.Username, dbo.TBUser.Password, 
                         dbo.TBUser.Role, dbo.TBUser.Flag, dbo.TBUser.CreatedBy, dbo.TBUser.IDCompany, dbo.TBUser.IDPosition, dbo.TBUser.CreatedAt
FROM            dbo.TBUser INNER JOIN
                         dbo.TB_Company ON dbo.TBUser.IDCompany = dbo.TB_Company.ID INNER JOIN
                         dbo.TB_Position ON dbo.TBUser.IDPosition = dbo.TB_Position.ID INNER JOIN
                         dbo.TB_Document ON dbo.TB_Company.ID = dbo.TB_Document.IDCompany INNER JOIN
                         dbo.TB_DocumentCatagory ON dbo.TB_Document.IDCategory = dbo.TB_DocumentCatagory.ID
GO
ALTER TABLE [dbo].[TB_Document]  WITH CHECK ADD  CONSTRAINT [FK_TB_Document_TB_Company] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[TB_Company] ([ID])
GO
ALTER TABLE [dbo].[TB_Document] CHECK CONSTRAINT [FK_TB_Document_TB_Company]
GO
ALTER TABLE [dbo].[TB_Document]  WITH CHECK ADD  CONSTRAINT [FK_TB_Document_TB_DocumentCatagory] FOREIGN KEY([IDCategory])
REFERENCES [dbo].[TB_DocumentCatagory] ([ID])
GO
ALTER TABLE [dbo].[TB_Document] CHECK CONSTRAINT [FK_TB_Document_TB_DocumentCatagory]
GO
ALTER TABLE [dbo].[TBUser]  WITH CHECK ADD  CONSTRAINT [FK_TBUser_TB_Company] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[TB_Company] ([ID])
GO
ALTER TABLE [dbo].[TBUser] CHECK CONSTRAINT [FK_TBUser_TB_Company]
GO
ALTER TABLE [dbo].[TBUser]  WITH CHECK ADD  CONSTRAINT [FK_TBUser_TB_Position] FOREIGN KEY([IDPosition])
REFERENCES [dbo].[TB_Position] ([ID])
GO
ALTER TABLE [dbo].[TBUser] CHECK CONSTRAINT [FK_TBUser_TB_Position]
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateCompany]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateCompany]
	-- Add the parameters for the stored procedure here
	
	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[TB_Company]
	(
		[UID],
		[Name],
		[Address],
		[Email],
		[Telephone],
		[Flag],
		[CreatedBy],
		[CreatedAt]
	)
	VALUES
	(
		NEWID(),
		@name,
		@address,
		@email,
		@telephone,
		@flag,
		@createdby,
		GETDATE()
	)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDocument]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateDocument]
	-- Add the parameters for the stored procedure here
	@idCompany int,
	@idCategory int,
	@name varchar(255),
	@description text,
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[TB_Document]
	(
		[UID],
		[IDCompany],
		[IDCategory],
		[Name],
		[Description],
		[Flag],
		[CreatedBy],
		[CreatedAt]
	)
	VALUES
	(
		NEWID(),
		@idCompany,
		@idCategory,
		@name,
		@description,
		@flag,
		@createdby,
		GETDATE()
	)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDocumentCategory]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateDocumentCategory]
	-- Add the parameters for the stored procedure here
	
	@name varchar(255),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[TB_DocumentCatagory]
	(
		[UID],
		[Name],
		[CreatedBy],
		[CreatedAt]
	)
	VALUES
	(
		NEWID(),
		@name,
		@createdby,
		GETDATE()
	)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_CreatePosition]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreatePosition]
	-- Add the parameters for the stored procedure here
	@name varchar(255),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[TB_Position]
	(
		[UID],
		[Name],
		[CreatedBy],
		[CreatedAt]
	)
	VALUES
	(
		NEWID(),
		@name,
		@createdby,
		GETDATE()
	)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_CreateUser]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateUser]
	-- Add the parameters for the stored procedure here
	@idCompany int,
	@idPosition int,
	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@username varchar(50),
	@password varchar(255),
	@role varchar(50),
	@Flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[TBUser]
	(
		[UID],
		[IDCompany],
		[IDPosition],
		[Name],
		[Address],
		[Email],
		[Telephone],
		[Username],
		[Password],
		[Role],
		[Flag],
		[CreatedBy],
		[CreatedAt]
	)
	VALUES
	(
		NEWID(),
		@idCompany,
		@idPosition,
		@name,
		@address,
		@email,
		@telephone,
		@username,
		CONVERT(varchar(32), HASHBYTES('MD5', @password), 2),
		@role,
		@Flag,
		@createdby,
		GETDATE()
	)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteCompany]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE
	FROM

	[dbo].[TB_Company] 
		
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteDocument]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteDocument]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE
	FROM

	[dbo].[TB_Document] 
		
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteDocumentCategory]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteDocumentCategory]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE
	FROM

	[dbo].[TB_DocumentCatagory] 
		
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeletePosition]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeletePosition]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE
	FROM

	[dbo].[TB_Position] 
		
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteUser]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteUser]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE
	FROM

	[dbo].[TBUser] 
		
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_loginUser]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_loginUser]
	@email varchar(50),
	@password varchar(50),
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	SELECT
		[ID],
		[Username],
		[Email],
		[Role],
		[CreatedAt]
	FROM TBUser
	WHERE
		[Email] = @email and [Password] = CONVERT(varchar(32), HASHBYTES('MD5', @password), 2)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateCompany]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[TB_Company] SET
	
		[Name] = @name,
		[Address] = @address,
		[Email] = @email,
		[Telephone] = @telephone,
		[Flag] = @flag,
		[CreatedBy] = @createdby
		
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDocument]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateDocument]
	-- Add the parameters for the stored procedure here
	@id int,
	@idCompany int,
	@idCategory int,
	@name varchar(255),
	@description text,
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[TB_Document] SET
		[IDCompany] = @idCompany,
		[IDCategory] = @idCategory,
		[Name] = @name,
		[Description] = @description,
		[Flag] = @flag,
		[CreatedBy] = @createdby
		
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDocumentCategory]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateDocumentCategory]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(255),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[TB_DocumentCatagory] SET
		
		[Name] = @name,
		[CreatedBy] = @createdby
		
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdatePosition]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdatePosition]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(255),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[TB_Position] SET
		[Name] = @name,
		[CreatedBy] = @createdby
		
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUser]    Script Date: 04/08/2023 11:16:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateUser]
	-- Add the parameters for the stored procedure here
	@id int,
	@idCompany int,
	@idPosition int,
	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@username varchar(50),
	@password varchar(255),
	@role varchar(50),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[TBUser] SET
	
		[IDCompany] = @idcompany,
		[IDPosition] = @idPosition,
		[Name] = @name,
		[Address] = @address,
		[Email] = @email,
		[Telephone] = @telephone,
		[Username] = @username,
		[Password] = CONVERT(varchar(32), HASHBYTES('MD5', @password), 2),
		[Flag] = @flag,
		[CreatedBy] = @createdby
		
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TB_Document"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "TB_DocumentCatagory"
            Begin Extent = 
               Top = 80
               Left = 245
               Bottom = 210
               Right = 415
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TB_Company"
            Begin Extent = 
               Top = 0
               Left = 456
               Bottom = 130
               Right = 626
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Document'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Document'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[48] 4[13] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBUser"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "TB_Company"
            Begin Extent = 
               Top = 0
               Left = 244
               Bottom = 130
               Right = 414
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "TB_Position"
            Begin Extent = 
               Top = 151
               Left = 234
               Bottom = 281
               Right = 404
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TB_Document"
            Begin Extent = 
               Top = 0
               Left = 551
               Bottom = 130
               Right = 721
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TB_DocumentCatagory"
            Begin Extent = 
               Top = 62
               Left = 815
               Bottom = 192
               Right = 985
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1410
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 13' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'50
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User'
GO
USE [master]
GO
ALTER DATABASE [PreTestHamdan] SET  READ_WRITE 
GO
