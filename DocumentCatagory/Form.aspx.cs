﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class DocumentCatagory_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerDocumentCatagory controllerDocumentCatagory = new ControllerDocumentCatagory(db);

                var documentCatagory = controllerDocumentCatagory.Cari(Request.QueryString["uid"]);

                if (documentCatagory != null)
                {
                    InputName.Text = documentCatagory.Name;

                    ButtonOK.Text = "Update";
                    LabelTitle.Text = "Update documentCatagory";
                }
                else
                {
                    ButtonOK.Text = "Add New";
                    LabelTitle.Text = "Add New documentCatagory";
                }
            }
        }
    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerDocumentCatagory controllerDocumentCatagory = new ControllerDocumentCatagory(db);

                if (ButtonOK.Text == "Add New")
                    controllerDocumentCatagory.Create(InputName.Text);
                else if (ButtonOK.Text == "Update")
                    controllerDocumentCatagory.Update(Request.QueryString["uid"], InputName.Text);

                db.SubmitChanges();

                Response.Redirect("./Default.aspx");
            }

        }
    }

    protected void ButtonKeluar_Click(object sender, EventArgs e)
    {
        Response.Redirect("./Default.aspx");
    }
}