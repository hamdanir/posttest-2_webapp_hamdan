﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DocumentCatagory_Default : System.Web.UI.Page
{

    DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                LoadData(db);
            }
        }
    }

    public void LoadData(DataClassesDatabaseDataContext db)
    {
        ControllerDocumentCatagory documentCatagory = new ControllerDocumentCatagory(db);
        repeaterDocumentCatagory.DataSource = documentCatagory.Data();
        repeaterDocumentCatagory.DataBind();
    }




    protected void repeaterDocumentCatagory_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/DocumentCatagory/Form.aspx?uid=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var documentCatagory = db.TB_DocumentCatagories.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                db.TB_DocumentCatagories.DeleteOnSubmit(documentCatagory);
                db.SubmitChanges();

                LoadData(db);
            }
        }
    }
}